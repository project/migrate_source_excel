<?php

namespace Drupal\migrate_source_excel\Plugin\migrate\source;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate\Plugin\migrate\source\SourcePluginBase;

/**
 * Source for Excel.
 *
 * If the Excel file contains non-ASCII characters, make sure it includes a
 * UTF BOM (Byte Order Marker) so they are interpreted correctly.
 *
 * @MigrateSource(
 *   id = "excel"
 * )
 */
class Excel extends SourcePluginBase {

  /**
   * List of available source fields.
   *
   * Keys are the field machine names as used in field mappings, values are
   * descriptions.
   *
   * @var array
   */
  protected $fields = [];

  /**
   * List of key fields, as indexes.
   *
   * @var array
   */
  protected $keys = [];

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);

    // Path is required.
    if (empty($this->configuration['path'])) {
      throw new MigrateException('You must declare the "path" to the source excel file in your source settings.');
    }

    // Key field(s) are required.
    if (empty($this->configuration['keys'])) {
      throw new MigrateException('You must declare "keys" as a unique array of fields in your source settings.');
    }

  }

  /**
   * Return a string representing the source query.
   *
   * @return string
   *   The file path.
   */
  public function __toString() {
    return $this->configuration['path'];
  }

  /**
   * {@inheritdoc}
   */
  public function initializeIterator() {
    $xls_reader = \PHPExcel_IOFactory::createReaderForFile($this->configuration['path']);
    $xls_data = $xls_reader->load($inputFileName);
    $data = array();
    $rowCount = 0;
    foreach ($xls_data->getWorksheetIterator() as $worksheet) {
      foreach ($worksheet->getRowIterator() as $row) {
        $cells = $row->getCellIterator();
        foreach ($cells as $cell) {
          // print_r($cell->getValue()); echo ',';
          $data[$rowCount][] = $cell->getValue();
        }
        // print '<br>';
        $rowCount++;
      }
    }
    $data = [['id' => 1, 'col' => 'abc'], ['id' => 2, 'col' => 'abc2'], ['id' => 3, 'col' => 'abc3']];
    $iterator = new \ArrayIterator($data);
    return $iterator;
  }
  
  /**
   * {@inheritdoc}
   */
  public function getIDs() {
    $ids = [];
    foreach ($this->configuration['keys'] as $key) {
      $ids[$key]['type'] = 'string';
    }
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [];
    foreach ($this->getIterator()->getColumnNames() as $column) {
      $fields[key($column)] = reset($column);
    }

    if (!empty($this->configuration['fields'])) {
      $fields = $this->configuration['fields'] + $fields;
    }

    return $fields;
  }

}
